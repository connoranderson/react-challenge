import React, { Component } from 'react';
import './styles/app.css';
import { Input, Card, message } from 'antd';
import fetchJsonp from "fetch-jsonp";

// ** Disclaimer **
// The information chosen to display for the MP was the postal code, city, province and email.
// One could easily argue that there should be more information, and I would not disagree.
// I chose these pieces of information because in this use-case the user just wants to know
// where the this MP is located and the email address to contact them.


class App extends Component {

  constructor(props){
    super(props);
    this.state = {
      postalCode: null,
      mpName: null,
      mpEmail: null,
      mpPostalCode: null,
      mpCity: null,
      mpProvince: null
    };
  }

  // Function to validate the input for the postal code field. This takes into account the 2 cases. Postal Code
  // (1) without a space (2) with a space. This will set the isValid bool. if Valid will call the fetch functio
  // and reset the input field.
  validatePostalCode(){
    if (this.state.postalCode !== null){
      let postalCode = this.state.postalCode.toUpperCase();
      // If the user has entered an input that is 7 characters long, the only way in which it is valid is if
      // itcontains a 'space' in the 4th character, this is removing that 'space'.
      if (this.state.postalCode.length === 7){
        // Only do this slice if the 4th char IS a space, otherwise input like T2PP5J4 would be changed.
        if (postalCode.charAt(3) === ' '){
          postalCode = postalCode.slice(0, 3) + postalCode.slice(4, postalCode.length)
        }
      }
      const postalCodeRegx = /[ABCEGHJKLMNPRSTVXY][0-9][ABCEGHJKLMNPRSTVWXYZ][0-9][ABCEGHJKLMNPRSTVWXYZ][0-9]/;
      let isValid = postalCodeRegx.test(postalCode);
      if (isValid){
        this.fetchPostalCodeInfo(postalCode);
        this.setState({postalCode: null});
      } else{
        message.warning('Please Enter A Valid Postal Code.');
      }
    }
  }

  // Function to return 2 different cards based on the state of having an MP fetched.
  // the mpName is being used to check this, it will be null if there isnt a fetched
  // mp, and will have a non-null value if fetched. This fucntion would normally be pulled
  // apart and put into a /components folder to be reused. Howver given the scope I have chose to
  // keep it in the App.js file.
  getMpCard(){
    if (this.state.mpName === null){
      return(
        <Card loading title="Your MP Info Here..." style={{ width: '290px' }}>
          Placeholder
        </Card>
      )
    } else {
      return(
        <Card className='mpCard' title={this.state.mpName} style={{ width: '290px' }}>
          <span> {this.state.mpPostalCode} </span>
          <span> {this.state.mpCity} , {this.state.mpProvince} </span>
          <span> <i className="fa fa-envelope" /> {this.state.mpEmail} </span>
        </Card>
      )
    }
  }

  fetchPostalCodeInfo(postalCode){
    // Setting 'this' Alis
    let component = this;
    //API call using Jsonp
    fetchJsonp(`https://represent.opennorth.ca/postcodes/${postalCode}`)
    .then(function(response) {
      return response.json();
    }).then(function(json) {
      // success case. Iterate Over the representatives_centroid array to find the MP
      json.representatives_centroid.forEach(function(key, i){
        let representative = json.representatives_centroid[i];
        // Once the MP is found in the representatives_centroidarray it sets state for
        // the required info about the MP. In an app any larger I would pull out this
        // function and have this update the redux store. I would then access the representative
        // information through something along the lines of this.props.mpInformation which would
        // default to null
        if (representative.elected_office === "MP") {
          component.setState({mpName: representative.name, mpEmail: representative.email, mpPostalCode: json.code, mpCity: json.city, mpProvince: json.province});
          message.success('MP Found.');
        }
      })
    }).catch(function(ex) {
      // This is the case where the Regx has deemed the postal code valid, however the postal code does
      // not have an endpoint.
      message.error('No Info Exists for this Postal Code.');
    })
  }

  render() {
    return (
      <div className="appPage">
        <img className='bubblesSVG' src='/bubbles.svg'></img>

        <div className='header'>
          <span> MP Finder </span>
        </div>
        <div className='infoSection flexColumnStart'>
          <Input value={this.state.postalCode} onChange={(e)=>{this.setState({postalCode: e.target.value})}} onPressEnter={()=>{this.validatePostalCode()}} className='antInput' size="large" placeholder="Enter Postal Code..." />
          {this.getMpCard()}
        </div>
        <div className='footer'> React Tech Challenge - Servall Dev - Connor Anderson </div>
      </div>
    )
  }
}

export default App;
